import ballerina/grpc;

listener grpc:Listener ep = new (9090);

@grpc:ServiceDescriptor {descriptor: ROOT_DESCRIPTOR_VINDI, descMap: getDescriptorMapVindi()}
service "RouteGuide" on new grpc:Listener(8980) {

    remote function leaner(RouteGuideRouteSummaryCaller caller, stream<Point, grpc:Error?> clientStream) returns error? {
        Point? lastPoint = ();
        int pointCount = 0;
        int featureCount = 0;
        int distance = 0;

        decimal startTime = time:monotonicNow();
        check clientStream.forEach(function(Point p) {
            pointCount += 1;
            if pointExistsInFeatures(FEATURES, p) {
                featureCount += 1;
            }

            if lastPoint is Point {
                distance = calculateDistance(<Point>lastPoint, p);
            }
            lastPoint = p;
        });
        decimal endTime = time:monotonicNow();
        int elapsedTime = <int>(endTime - startTime);
        return caller->sendRouteSummary({point_count: pointCount, feature_count: featureCount, distance: distance, elapsed_time: elapsedTime});
    }
}service "RouteGuide" on new grpc:Listener(8980) {

    remote function ListFeatures(RouteGuideFeatureCaller caller, Rectangle rectangle) returns error? {

        foreach Feature feature in FEATURES {
            if inRange(feature.location, rectangle) {
                check caller->sendFeature(feature);
            }
        }
    }
}
